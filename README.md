# flutter_rickymorty_pos

Projeto de listagem dos personagens do Ricky and Morty

# Tem 3 telas
 * Listagem dos personagens
 * Detalhamento dos personagens
 * Tela de contato

# Navegação
![](https://gitlab.com/resplandeluiz/flutter-pos/-/raw/e953bdd27ad2ec6081f02ee7648163fba6a5328b/lib/gif/gif.gif)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
