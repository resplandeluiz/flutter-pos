import 'package:flutter/material.dart';
import 'package:flutter_rickymorty_pos/contactpage.dart';
import 'package:flutter_rickymorty_pos/detailpage.dart';

import 'package:dio/dio.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    initialRoute: '/',
    routes: {
      '/': (context) => const MyHomePage(),
      '/detail': (context) => const DetailPage(
            person: null,
          ),
      '/contact': (context) => const ContactPage(),
    },
  ));
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePAgeState();
}

class _MyHomePAgeState extends State<MyHomePage> {
  var jsonList = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    try {
      var response =
          await Dio().get("https://rickandmortyapi.com/api/character?page=1");
      if (response.statusCode == 200) {
        setState(() {
          jsonList = response.data['results'] as List;
        });
      } else {
        print(response.statusCode);
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ricky and Morty App"),
        backgroundColor: const Color(0xFFA7CB54),
        actions: <Widget>[
          ElevatedButton(
            child: const Icon(
              Icons.contact_mail,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/contact');
            },
          )
        ],
      ),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return Container(
            padding: const EdgeInsets.all(0),
            child: Card(
              child: ListTile(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailPage(person: jsonList[index]),
                  ),
                ),
                leading: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      image: DecorationImage(
                        image: NetworkImage(jsonList[index]!['image']),
                        fit: BoxFit.contain,
                        alignment: Alignment.topCenter,
                      )),
                ),
                title: Text(jsonList[index]['name']),
                subtitle: Text(jsonList[index]!['location']!['name']),
              ),
            ),
          );
        },
        itemCount: jsonList == null ? 0 : jsonList.length,
      ),
    );
  }
}
