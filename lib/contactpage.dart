import 'package:flutter/material.dart';

class ContactPage extends StatelessWidget {
  const ContactPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Contatos"),
        backgroundColor: const Color(0xFFA7CB54),
      ),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Center(
              child: Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                    image: const DecorationImage(
                      image: NetworkImage(
                        'https://media.licdn.com/dms/image/D4D03AQHKCGsSQz09Pg/profile-displayphoto-shrink_400_400/0/1687460671285?e=1693440000&v=beta&t=bNLDjnbeDHoNEanJxql6YpN8g4bQqm0pgfEtfCoJ0lY',
                      ),
                      fit: BoxFit.contain,
                      alignment: Alignment.topCenter,
                    ),
                    color: const Color(0xFFA7CB54),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  width: 300,
                  height: 300,
                  margin: const EdgeInsets.all(12)),
              const Column(children: [
                Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      "Luiz Manoel Resplande Oliveira",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      "luizresplandeoliveira@gmail.com",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      "GIT - resplandeluiz",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    )),
                    Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      "61 982707629",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    )),
              ]),
            ],
          ))),
    );
  }
}
