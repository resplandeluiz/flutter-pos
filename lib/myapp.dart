import 'package:flutter/material.dart';
import 'package:flutter_rickymorty_pos/detailpage.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}
