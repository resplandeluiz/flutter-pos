import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({super.key, required this.person});

  final person;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(person!['name']),
        backgroundColor: const Color(0xFFA7CB54),
        actions: <Widget>[
          ElevatedButton(
            child: const Icon(
              Icons.contact_mail,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/contact');
            },
          )
        ],
      ),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Center(
              child: Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(person!['image']),
                      fit: BoxFit.contain,
                      alignment: Alignment.topCenter,
                    ),
                    color: const Color(0xFFA7CB54),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: const Offset(0, 3),
                      ),
                    ],
                  ),
                  width: 350,
                  height: 350,
                  margin: const EdgeInsets.all(12)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                      padding: const EdgeInsets.all(15),
                      child: Text(
                        "Espécie: ${person!['species']}",
                        style: const TextStyle(
                          fontSize: 20,
                        ),
                      )),
                  Padding(
                      padding: const EdgeInsets.all(15),
                      child: Text(
                        "Status: ${person!['status']}",
                        style: const TextStyle(
                          fontSize: 20,
                        ),
                      )),
                  Padding(
                      padding: const EdgeInsets.all(15),
                      child: Text(
                        "Localização: ${person!['location']['name']}",
                        style: const TextStyle(
                          fontSize: 20,
                        ),
                      )),
                  Padding(
                      padding: const EdgeInsets.all(15),
                      child: Text(
                        "Apareceu em ${person!['episode'].length} ${person!['episode'].length == 1 ? 'espisódio' : 'espisódios'} ",
                        style: const TextStyle(
                          fontSize: 20,
                        ),
                      )),
                ],
              )
            ],
          ))),
    );
  }
}
